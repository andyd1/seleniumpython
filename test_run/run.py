import unittest
import os
import sys
from util.HTMLTestRunner import HTMLTestRunner
import datetime
from test_case.login_case import LoginCase
from config import setting
from util.get_last_report import GetLastReport

sys.path.append(setting.base_dir)

suite = unittest.TestLoader().loadTestsFromTestCase(LoginCase)
report_name = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '.html'
report_file = os.path.join(setting.report_path, report_name)
with open(report_file,'wb') as f:
    runner = HTMLTestRunner(stream=f, title='', description='', verbosity=2)
    runner.run(suite)
last_report = GetLastReport()
last_report_file = last_report.last_report_file()