from page.login_page import LoginPage


class LoginHandle(object):
    def __init__(self, driver):
        self.driver = driver
        self.lp = LoginPage(self.driver)

    # 输入用户名
    def send_username(self, username):
        self.lp.get_username_element().send_keys(username)

    # 输入用户名
    def send_password(self, password):
        self.lp.get_password_element().send_keys(password)

    # 点击登录按钮
    def click_login_btn(self):
        self.lp.login_btn_element().click()

    # 获取登录成功的账号文字
    def get_login_username_text(self):
        text = self.lp.get_usernametext_element().text
        print(text)
        return text