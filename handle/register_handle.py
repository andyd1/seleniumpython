from page.register_page import RegisterPage
from util.get_code_value import GetCode



class RegisterHandle(object):
    # 打开页面后自动输入相应信息
    def __init__(self, driver):
        self.driver = driver
        self.rp = RegisterPage(self.driver)

    # 输入用户邮箱
    def send_user_email(self, email):
        self.rp.get_email_element().send_keys(email)

    # 输入用户名
    def send_user_name(self, username):
        self.rp.get_email_element().send_keys(username)

    # 输入用户密码
    def send_user_password(self, password):
        self.rp.get_password_element().send_keys(password)

    # 输入验证码
    def send_user_code(self, file_name):
        get_code_text = GetCode(self.driver)
        code = get_code_text.code_online(file_name)
        self.rp.get_code_element().send_keys(code)

    # 获取文字信息
    def get_user_text(self, info, user_info):
        try:
            if info == 'user_email_error':
                text = self.rp.get_email_error_element().text  # 获取邮箱错误信息
                if text == user_info:
                    print('错误提示信息符合需求')
                else:
                    print('错误提示信息不符合需求')
            elif info == 'user_name_error':
                text = self.rp.get_username_error_element().text  # 获取用户名错误信息
                if text == user_info:
                    print('错误提示信息符合需求')
                else:
                    print('错误提示信息不符合需求')
            elif info == 'password_error':
                text = self.rp.get_password_error_element().text # 获取用户密码错误信息
                if text == user_info:
                    print('错误提示信息符合需求')
                else:
                    print('错误提示信息不符合需求')
            else:
                text = self.rp.get_code_error_element().text  # 获取验证码错误信息
                if text == user_info:
                    print('错误提示信息符合需求')
                else:
                    print('错误提示信息不符合需求')
        except:
            text = None
        return text

    # 点击注册按钮
    def click_register_button(self):
        self.rp.get_button_element().click()

    # 获取注册按钮文字
    def get_register_bn_text(self):
        # 如获取不到信息，表明页面已成功跳转
        return self.rp.get_button_element().text