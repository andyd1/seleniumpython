from handle.register_handle import RegisterHandle


class RegisterBusiness(object):
    # 测试注册页面form表单功能情况
    def __init__(self, driver):
        self.rh = RegisterHandle(driver)

    def user_base(self, email, username, password, file_name):
        self.rh.send_user_email(email)
        self.rh.send_user_name(username)
        self.rh.send_user_password(password)
        self.rh.send_user_code(file_name)
        self.rh.click_register_button()

    def register_succes(self):
        if self.rh.get_register_bn_text() == None:
            return True
        else:
            return False

    def register_email_error(self, email, username, password, file_name):
        self.user_base(email, username, password, file_name)
        if self.rh.get_user_text('user_email_error', '请输入有效的电子邮件地址') == None:
            # 注册成功
            return True
        else:
            return False

    def register_username_error(self, email, username, password, file_name):
        self.user_base(email, username, password, file_name)
        if self.rh.get_user_text('user_name_error', '字符长度必须大于等于4，一个中文字算2个字符') == None:
            return True
        else:
            return False

    def register_password_error(self,email, username, password, file_name):
        self.user_base(email, username, password, file_name)
        if self.rh.get_user_text('password_error', '最少需要输入 5 个字符'):
            return True
        else:
            return False

    def register_code_error(self, email, username, password, file_name):
        self.user_base(email, username, password, file_name)
        if self.rh.get_user_text('code_text_error', '验证码错误') == None:
            return True
        else:
            return False

    def register_fuction(self, email, username, password, file_name, assertCode, assertText):
        self.user_base(email, username, password, file_name)
        if self.rh.get_user_text(assertCode, assertText) == None:
            return True
        else:
            return False