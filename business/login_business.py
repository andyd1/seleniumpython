from handle.login_handle import LoginHandle


class LoginBusiness(object):

    def __init__(self, driver):
        self.lh = LoginHandle(driver)

    def base_user(self, username, password):
        self.lh.send_username(username)
        self.lh.send_password(password)
        self.lh.click_login_btn()

    # def login_sucess(self, text):
    #     if self.lh.get_login_username_text() == text:
    #         print('登录成功')
    #         return True
    #     else:
    #         print('登录失败')
    #         return False