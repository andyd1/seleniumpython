from selenium import webdriver


class OpenBrowser(object):
    def start_chrom_browser(self):
        driver = webdriver.Chrome()

    def start_firefix_browser(self):
        driver = webdriver.Firefox()

    def start_ie_browser(self):
        driver = webdriver.Ie()

    def start_edg_browser(self):
        driver = webdriver.Edge()