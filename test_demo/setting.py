#获取文件路径
import os

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) #项目首路径
code_error_path = os.path.join(base_dir, 'Image', 'code_error.png')
code_path = os.path.join(base_dir, 'Image', 'code.png')
config_ini_dir = os.path.join(base_dir, 'config', 'LocalElement.ini')