from selenium import webdriver
from selenium.webdriver.support import expected_conditions
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


class ExpectedCondition(object):
    # 判断页面标题是否与我们想要的一致
    def expected_condition(self):
        driver = webdriver.Chrome()
        driver.get('')
        time.sleep(5)
        is_live = expected_conditions.title_contains('')
        print('is_live:',is_live)

        # 判断元素是否可见
    def element_visible(self):
        driver = webdriver.Chrome()
        driver.get('')
        time.sleep(5)
        locator = (By.CLASS_NAME, '')# 通过classname的方式 找到 元素
        WebDriverWait(driver, 2).until(expected_conditions.visibility_of_element_located(locator))
        # WebDriverWait： 传入两个参数，一个是driver，另一个是超时时间(int类型)
        # visibility_of_element_located：只在可见的元素里找对应的元素。如有返回内存地址