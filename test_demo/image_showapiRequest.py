# 使用showapiRequest解决图片验证码识别
# 进入万维易源网：https://www.showapi.com/   进行搜索验证码
# 选择：验证码识别英数_文件 ，找到请求示例，下载SDK：ShowapiRequest.py
# python3.6.5
# 需要引入requests包 ：运行终端->进入python/Scripts ->输入：pip install requests
from ShowapiRequest import ShowapiRequest
#第一种方式
r = ShowapiRequest('http://route.showapi.com/26-4','my_appId','my_appSecret' )
r.addBodyPara('typeId', '35')#typeId：表示识别几位数的图片验证码 ；35：'3'表示英数结合的验证码，'5'表示5位数的验证码   ； 31：一位数的验证码
r.addBodyPara('convert_to_jpg','0')
r.addBodyPara('needMorePrecise','0')
r.addFilePara('img', r'E:\imooc2.png')
res = r.post()
print(res.text) # 返回信息 {"showapi_res_error":"","showapi_res_id":"6f0e4cdb977141b293ea12178ad3d37e","showapi_res_code":0,"showapi_res_body":{"Id":"5bac83cc-5637-4e2d-bc91-25a78b527241","Result":"ANLZZ","ret_code":0}}
text = res.json()['showapi_res_body']['Result']# 以json格式读取：showapi_res_body 下 Result 的值
print(text)# 返回信息：ANLZZ (识别正确)

