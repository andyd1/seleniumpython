#注册测试主程序
from selenium import webdriver
from test_demo.find_element import FindElement
import random
from PIL import Image
from ShowapiRequest import ShowapiRequest
from selenium.common.exceptions import NoSuchElementException
import time
from test_demo import setting


class RegisterFunction(object):
    def __init__(self, file_name, url, i, code_error_name):
        # 浏览器初始化
        self.i = i
        self.url = url
        self.file_name = file_name
        self.code_error_name = code_error_name
        self.driver = self.get_driver()

    # 启动浏览器类型
    def get_driver(self):
        if self.i == 1:
            driver = webdriver.Chrome()
        elif self.i == 2:
            driver = webdriver.Firefox()
        elif self.i == 3:
            driver = webdriver.Edge()
        else:
            driver = webdriver.Ie()
        driver.get(self.url)
        driver.maximize_window()
        return driver

    # 定位用户元素位置
    def get_user_element(self, key):
        find_element = FindElement(self.driver)
        user_element = find_element.get_element(key)
        return user_element

    def send_user_info(self, key, data):
        self.get_user_element(key).send_keys(data)

    def get_range_for_user(self):
        user_info = ''.join(random.sample('zaqwsxcderfvbgtyhnmjuik123456789', 8))
        return user_info

    def get_code_image(self):
        self.driver.save_screenshot(self.file_name)
        code_element = self.get_user_element('code_image')
        left_x = code_element.location['x']
        top_y = code_element.location['y']
        right_x = code_element.size('width') + left_x
        button_y = code_element.size('height') + top_y

        im  = Image.open(self.file_name)
        img = im.crop((left_x, top_y, right_x, button_y))
        img.save(self.file_name)

    def code_online(self):
        r = ShowapiRequest('http://route.showapi.com/184-4", "62626", "d61950be50dc4dbd9969f741b8e730f5')
        r.addBodyPara('typeId', 35)
        r.addBodyPara('convert_to_jpg', 0)
        r.addFilePara('image', self.file_name)
        res = r.post() # {"showapi_res_error":"","showapi_res_id":"6f0e4cdb977141b293ea12178ad3d37e","showapi_res_code":0,"showapi_res_body":{"Id":"5bac83cc-5637-4e2d-bc91-25a78b527241","Result":"ANLZZ","ret_code":0}}
        code_text = res.json()['showapi_res_body']['Result']
        return code_text

    def run_main(self):
        user_name = self.get_range_for_user()
        user_email = user_name + '@163.com'
        self.send_user_info('user_email', user_email)
        self.send_user_info('user_name', user_name)
        self.send_user_info('password', '12345678')
        self.get_code_image()
        code_text = self.code_online()
        self.send_user_info('code_text', code_text)
        self.get_user_element('register_button').click()
        try:
            code_error = self.get_user_element('code_text_error')
        except NoSuchElementException:
            print('注册成功')
        else:
            self.driver.save_screenshot(self.code_error_name)
        time.sleep(3)
        self.driver.close()

#调试
if __name__ == '__main__':
    file_name = setting.code_path
    code_error_name = setting.code_error_path
    url = 'http://www.5itest.cn/register'
    register = RegisterFunction(file_name, url, 1, code_error_name)
    register.run_main()