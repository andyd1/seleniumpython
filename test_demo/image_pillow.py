#将注册页面全屏截图保存（使用driver.save_screenhot方法），再将验证码部分区域截图保存下来 PIL
from selenium import webdriver
import os
from PIL import Image


driver = webdriver.Chrome()

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))#获取项目的目录,多一个os.path.dirname就返回多一级，os.path.abspath切换为\
base_dir1 = os.path.dirname(os.path.abspath(__file__))#获取文件所在目录
base_dir2 = os.path.dirname(os.path.dirname(__file__))#获取文件所在目录

image_path = os.path.join(base_dir, 'Image', 'imooc.png')
code_path = os.path.join(base_dir, 'Image', 'code.png')

driver.get('http://www.5itest.cn/register')
driver.save_screenshot(image_path)

code_element = driver.find_element_by_id('getcode_num')
print(code_element.location)#获取该元素左上角的（x，y）坐标
print(code_element.size)#获取该元素（图）长度、宽度

left_x = code_element.location['x']#左上角x值
top_y = code_element.location['y']#左上角y值
right_x = code_element.size['width'] + left_x #右下角x值
button_y = code_element.size['height'] + top_y #右下角y值

im = Image.open(image_path)# 使用PIL下的Image打开下载的imooc.png图片
img = im.crop((left_x, top_y, right_x, button_y))# 将指定的某部分裁剪出来
img.save(code_path)#保存