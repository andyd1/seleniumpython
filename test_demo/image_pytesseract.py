#安装：pip install pytesseract
#使用pytesseract识别图片中得问题
import pytesseract
from PIL import Image

image = Image.open('E:/imooc2.png')
text = pytesseract.image_to_string(image) # 将图片文字转换成字符串
print(text)

# 缺点：机械性读取，无法读取图片中不规则的字体，不适合于干扰性比较强的验证码图片文字读取