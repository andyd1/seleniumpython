#读取配置文件
from config.setting import config_ini_dir
import configparser


class ReadIni(object):
    #初始化
    def __init__(self, node = None):
        if node:
            self.node = node
        else:
            self.node = 'RegisterElement'
        self.cf = self.load_ini()

    #加载文件
    def load_ini(self):
        cf = configparser.ConfigParser()
        cf.read(config_ini_dir)
        return cf

    #获取配置文件中key的value值
    def get_value(self,key):
        data = self.cf.get(self.node, key)
        return data
