# excel表基本读写操作
import xlrd
from xlutils.copy import copy
import time
from config.setting import excel_default_path


class ExcelOpertion(object):
    # Excel表数据相关操作
    def __init__(self, ex_path=None, index=None):
        if ex_path is None:
            self.excel_path = excel_default_path # 默认excel文件路径
        else:
            self.excel_path = ex_path
        if index is None:
            index = 0
        self.data = xlrd.open_workbook(self.excel_path)
        self.table = self.data.sheets()[index] # sheets第几页数据

    # 获取excel行数
    def get_lines(self):
        rows = self.table.nrows
        if rows > 1:
            return rows
        return None

    # 获取excel数据，按照每行一个list，添加到一个大的list里面
    def get_data(self):
        result = []
        rows = self.get_lines()
        if rows is not None:
            for i in range(1, rows):
                row = self.table.row_values(i)
                result.append(row)
            return result
        return None

    # 获取单元格的数据
    def get_col_value(self, row, col):
        if self.get_lines() > row:
            data = self.table.cell(row, col).value

    # 写入数据
    def write_value(self, row, value):
        read_value = xlrd.open_workbook(self.excel_path) # 打开excel文件
        write_data = copy(read_value)
        write_data.get_sheet(0).write(row, 9, value) # 获取excel表首页数据，并在row行9列写入数据
        write_data.save(self.excel_path)
        time.sleep(5)