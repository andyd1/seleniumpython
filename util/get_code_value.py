from PIL import Image
import time
from ShowapiRequest import ShowapiRequest


class GetCode:
    #获取验证码图片，解析验证码图片并返回验证码值
    def __init__(self, driver):
        self.driver = driver

    def get_image_code(self, file_name):
        self.driver.save_screenshot(file_name)
        code_element = self.driver.find_element_by_id('getcode_num')
        left = code_element.location['x']
        top = code_element.location['y']
        right = code_element.size['width'] + left
        button = code_element.size['height'] + top
        im = Image.open(file_name)
        img = im.crop((left, top, right, button))
        img.save(file_name)
        time.sleep(5)

    #解析图片获取验证码
    def code_online(self, file_name):
        self.get_image_code(file_name)
        r = ShowapiRequest('http://route.showapi.com/26-4', 'my_appId', 'my_appSecret')
        r.addBodyPara('typeId', '35')  # typeId：表示识别几位数的图片验证码 ；35：'3'表示英数结合的验证码，'5'表示5位数的验证码   ； 31：一位数的验证码
        r.addBodyPara('convert_to_jpg', '0')
        r.addBodyPara('needMorePrecise', '0')
        r.addFilePara('img', r'E:\imooc2.png')
        res = r.post()
        print(res.text)  # 返回信息 {"showapi_res_error":"","showapi_res_id":"6f0e4cdb977141b293ea12178ad3d37e","showapi_res_code":0,"showapi_res_body":{"Id":"5bac83cc-5637-4e2d-bc91-25a78b527241","Result":"ANLZZ","ret_code":0}}
        text = res.json()['showapi_res_body'] # 以json格式读取：showapi_res_body 下 Result 的值
        print(text)  # 返回信息：ANLZZ (识别正确)
        try:
            code = text['Result']
        except Exception as e:
            print('code_error:',e)
            return None