# 读取excel表中各列的数据
from util.excel_operation import ExcelOpertion
import xlrd
from xlutils.copy import copy
import time


class GetCellsValue(object):
    # 获取keyword.xls表单元格数据
    def __init__(self, excel_path):
        self.case_id = 0    # id
        self.case_name = 1   # 模块名称
        self.action_type = 2  # 操作类型：打开浏览器、输入用户名...
        self.is_run = 3       # 是否执行
        self.action_method = 4   # 执行方法
        self.send_value = 5    # 发送的数据
        self.oper_element = 6   # 操作元素
        self.expect_result = 7  # 预期结果
        self.real_result = 8    # 实际结果
        self.report_result = 9   # 实际报告，是否通过

        self.excel_path = excel_path
        self.excel_oper = ExcelOpertion(self.excel_path)
        self.get_lines = self.excel_oper.get_lines()

    # 获取case_id
    def get_case_id(self, row):
        case_id = self.excel_oper.get_col_value(row, self.case_id)
        return case_id

    # 获取模块名称
    def get_case_name(self, row):
        case_name = self.excel_oper.get_col_value(row, self.case_name)
        return case_name

    # 获取是否执行
    def get_is_run(self, row):
        is_run = self.excel_oper.get_col_value(row, self.is_run)
        return is_run

    # 获取执行方法
    def get_action_method(self, row):
        action_method = self.excel_oper.get_col_value(row, self.action_method)
        return action_method

    # 获取发送的数据
    def get_send_value(self, row):
        send_value = self.excel_oper.get_col_value(row, self.send_value)
        return send_value

    # 获取操作元素
    def get_oper_element(self, row):
        oper_element = self.excel_oper.get_col_value(row, self.oper_element)
        return oper_element

    # 获取预期结果
    def get_expect_result(self, row):
        expect_result = self.excel_oper.get_col_value(row, self.expect_result)
        return expect_result

    # 获取实际结果
    def get_real_result(self, row):
        real_result = self.excel_oper.get_col_value(row, self.real_result)
        return real_result

    # 获取报告
    def get_report_result(self, row):
        report_result = self.excel_oper.get_col_value(row, self.report_result)
        return report_result

    def write_cell_value(self, row, value):
        # 在指定单元格写入数据
        self.excel_oper.write_value(row, self.report_result, value)

    # 写入数据  #excel_operation.py文件
    def write_value(self, row, col, value):
        read_value = xlrd.open_workbook(self.excel_path)  # 打开excel文件
        write_data = copy(read_value)  # 复制文件，让xlutils模块相关操作表
        write_data.get_sheet(0).write(row, col, value)  # 获取excel表首页数据，并在row行9列写入数据
        write_data.save(self.excel_path)  # 保存
        time.sleep(1)
