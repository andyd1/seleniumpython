from  selenium import webdriver
from base.find_element import FindElement


class ActionMethod(object):

    def open__browser(self, browser):
        try:
            if browser == 'chrome':
                self.driver = webdriver.Chrome()
            elif browser == 'firefox':
                self.driver = webdriver.Firefox()
            elif browser == 'ie':
                self.driver = webdriver.Ie()
            else:
                self.driver = webdriver.Edge()
        except:
            print('ActionMethodError：没有"{}"这个元素".format(browser)')

    def get_url(self, url):
        try:
            self.driver.get(url)
        except:
            print('ActionMethodError：url：{}，输入有误'.format(url))

    def get_element(self, key):
        try:
            find_element = FindElement(self.driver)
            element = find_element.get_element(key)
            return element
        except:
            print('ActionMethodError："{}"元素定位失败'.format(key))

    def element_send_keys(self, value, key):
        try:
            element = self.get_element(key)
            element.send_keys(value)
        except:
            print('ActionMethodError：输入有误："{}"'.format(value))

    def click_element(self, key):
        try:
            element = self.get_element(key)
            element.click()
        except:
            print('ActionMethodError："{}"元素不存在，无法点击'.format(key))