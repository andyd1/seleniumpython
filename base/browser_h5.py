from selenium import webdriver
from time import sleep
from selenium.webdriver.common.action_chains import ActionChains


def browser_h5():
    WIDTH = 320
    HEIGHT = 640
    PIXEL_RATIO = 3.0
    UA = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Mobile Safari/537.36'
    mobileEmulation = {"deviceMetrics": {"width": WIDTH, "height": HEIGHT, "pixelRatio": PIXEL_RATIO}, "userAgent": UA}
    options = webdriver.ChromeOptions()
    options.add_experimental_option('mobileEmulation', mobileEmulation)
    driver = webdriver.Chrome(executable_path='chromedriver.exe', chrome_options=options)
    driver.get('https://my9696.com/#/home')
    driver.maximize_window()
    sleep(5)

    element_lt = driver.find_element_by_xpath('/html/body/div[3]/div/div[2]/div/div/div/div/div/p')
    element_lt.click()
    sleep(1)

    image_element1 = driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[1]/div/div[2]/div/div/div[1]/div/div/div[6]/img')
    image_element2= driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[1]/div/div[2]/div/div/div[1]/div/div/div[4]/img')
    image_element3= driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[1]/div/div[2]/div/div/div[1]/div')

    w = image_element1.location['x']
    h = image_element1.location['y']
    print(w,h)
    x1 = int(image_element1.size['width'] * 0.25 + w)
    y1 = int(image_element1.size['height'] * 0.5 + h)
    x2 = int(image_element1.size['width'] * 0.9 + w)
    y2 = int(image_element1.size['height'] * 0.5 + h)
    # print(element_lt.text)
    print(x1,y1,x2,y2)
    # driver.execute_script("arguments[0].scrollIntoView();", element_lt)
    for i in range(10):
        # driver.execute_script("arguments[0].scrollIntoView();", image_element1)
        Action = ActionChains(driver)
        Action.drag_and_drop(image_element2, image_element1).perform()
        sleep(3)
        # ActionChains(driver).move_by_offset(x1, y1).click().perform()
        # ActionChains(driver).move_by_offset(x2, y2).click().perform()
    # drawing.perform()
    cancle_btn = driver.find_element_by_xpath('//*[@id="app"]/div[1]/div[1]/div/div[3]/div/div[2]/div/div[1]/span')
    cancle_btn.click()
    sleep(3)
    driver.quit()

if __name__ == '__main__':
    browser_h5()