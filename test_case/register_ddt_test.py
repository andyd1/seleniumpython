import ddt
import unittest
from selenium import webdriver
from config import setting
from business.register_business import RegisterBusiness
import os
from util.excel_operation import ExcelOpertion
from util.HTMLTestRunner import HTMLTestRunner
import datetime
from util.get_last_report import GetLastReport

ex_opr = ExcelOpertion()
ex_data = ex_opr.get_data()


@ddt.ddt
class RegisterDdtCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get('http://www.5itest.cn/register')
        cls.driver.maximize_window()
        cls.file_name = setting.code_path
        cls.register = RegisterBusiness(cls.driver)

    def setUp(self):
        self.driver.refresh()

    def tearDown(self):
        for method_name, error in self._outcome.errors:
            if error:

                report_error_name = self.assertCode + '.png'
                report_error_path = os.path.join(setting.base_dir, 'report',report_error_name)
                self.driver.save_screenshot(report_error_path)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

    @ddt.data(*ex_data)
    @unittest.skip('')
    def test_register_case(self, ex_data):
        email, username, password, self.assertCode, assertText = ex_data
        register_error = self.register.register_fuction(email, username, password , self.file_name,self.assertCode, assertText)
        self.assertFalse(register_error,'测试失败：{}'.format(self.assertCode))

# if __name__ == '__main__':
#     suite = unittest.TestLoader().loadTestsFromTestCase(RegisterDdtCase)
#     report_name = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + '.html'
#     report_file = os.path.join(setting.report_path, report_name)
#     with open(report_file) as  f:
#         runner = HTMLTestRunner(stream=f, title='', description='', verbosity=2)
#         runner.run(suite)
#     last_report = GetLastReport()
#     last_report_file = last_report.last_report_file()