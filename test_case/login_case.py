import unittest
from selenium import webdriver
from business.login_business import LoginBusiness
import time


class LoginCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get('https://ht.my9696.com/pyqct/login')
        cls.driver.maximize_window()

    # def setUp(self):
    #     print('eee')

    # def tearDown(self):
    #     self.driver.close()

    def test_login_case(self):
        self.lg = LoginBusiness(self.driver)
        self.lg.base_user('andy', '123456')
        # self.lg.login_sucess('andy')

    def test_logout_case(self):
        username_element = self.driver.find_element_by_link_text('andy')
        print(username_element.text)
        username_element.click()
        self.driver.find_element_by_link_text('退出').click()
        time.sleep(5)
        self.driver.close()