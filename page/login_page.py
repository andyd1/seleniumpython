from base.find_element import FindElement


class  LoginPage(object):
    def __init__(self, driver):
        self.lp = FindElement(driver)

    # 获取用户名元素
    def get_username_element(self):
        return self.lp.get_element('user_name')

    # 获取密码元素
    def get_password_element(self):
        return self.lp.get_element('password')

    # 获取登录按钮
    def login_btn_element(self):
        return self.lp.get_element('login_btn')

    # 获取登录后用户账号
    def get_usernametext_element(self):
        return self.lp.get_element('login_username_text')

